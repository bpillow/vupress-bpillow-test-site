module.exports = {
  title: 'GitLab ❤️ VuePress - Bpillow Test',
  description: 'Vue-powered static site generator running on GitLab Pages...and more!',
  base: '/vupress-bpillow-test-site/',
  dest: 'public'
};
